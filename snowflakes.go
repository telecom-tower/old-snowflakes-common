package snowflakes

//go:generate esc -o html.go -prefix html html

import (
	"math/rand"
	"time"

	"gitlab.com/telecom-tower/pixtrix"
)

type Renderer interface {
	SetBitmap([]uint32)
	Render() error
	Wait() error
}

type flake struct {
	pos    int
	height int
	delay  int
	color  uint32
}

func MakeSnow(r Renderer, rows int, columns int) {

	frames := make([]*pixtrix.Pixtrix, 3)

	for i := range frames {
		frames[i] = &pixtrix.Pixtrix{
			Rows:   rows,
			Bitmap: make([]uint32, rows*columns),
		}
	}

	frameNr := make(chan int)
	flakes := make([]flake, columns)

	go func() {
		i := 0
		for {
			// generate k flakes
			for k := 0; k < 2; k++ {
				x := rand.Intn(columns)
				if flakes[x].height == 0 {
					flakes[x].height = 2 + rand.Intn(2)
					flakes[x].pos = 0
					flakes[x].delay = 2 + rand.Intn(4)
					rg := 255 - 32 + uint32(rand.Intn(32))
					flakes[x].color = rg<<16 + rg<<8 + rg
				}
			}

			// Clear

			for x := 0; x < columns; x++ {
				for y := 0; y < rows; y++ {
					if y <= flakes[x].pos && y > flakes[x].pos-flakes[x].height {
						frames[i].SetPixel(x, y, flakes[x].color)
					} else {
						frames[i].SetPixel(x, y, 0x000000)
					}
				}
				if flakes[x].height > 0 {
					flakes[x].pos++
					if flakes[x].pos-flakes[x].height-flakes[x].delay > rows {
						flakes[x].pos = 0
						flakes[x].height = 0
						flakes[x].delay = 0
					}
				}
			}
			frameNr <- i
			i = (i + 1) % 3
		}
	}()

	for x := 0; x < columns; x++ {
		frames[0].SetPixel(x, 2, 0xffffff)
	}

	go func() {
		for {
			i := <-frameNr
			r.SetBitmap(frames[i].InterleavedStripes()[0])
			r.Render()
			r.Wait()
			time.Sleep(50 * time.Millisecond)
		}
	}()

}
